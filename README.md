# Simple Maps

Mit dieser Contao-Erweiterung lassen sich einfache Karten mit Pins erstellen. Diese kann man dann entweder per Inhalts-Element oder als Frontend-Modul auf der Website einbinden. Die eingesetzte Technologie basiert auf OSM (Open-Street-Map) - Kartenmatrial, optional Mapbox (Layer-API) und Leaflet (JavaScript library).

Der Einsatz von Mapbox ist optional. Wenn Mapbox in den Karten-Einstellungen nicht aktiviert wurde, wird die OSM-Karte angezeigt.

Es können auch Kategorien angelegt werden. Bei den Markierungen haben Sie die Möglichkeit, jeder einer Kategorie zuzuordnen. Es gibt ein Contend-Element und Frontend-Modul, bei denen die jeweiligen Marker-Elemente nach Kategorien gruppiert, angezeigt werden können. Mit aktiviertem accordion-Javascript wird es als Akkordion dargestellt.

Dies ist und soll auch zukünftig eine einfache Möglichkeit sein z.B. ein oder mehrere Standorte eines Unternehmens auf der Website darzustellen. Wenn Sie aber auf der Suche nach einer Contao-Erweiterung sind, die sehr viel granularer und mächtiger in Sachen Kartendarstellung ist, dann ist evtl. die Erweiterung "con4gis/maps" von "Kuestenschmiede GmbH Software & Design" oder "netzmacht/contao-leaflet-maps" von "David Molineus" was für Sie.

---

Icons: https://www.fatcow.com/free-icons    

OSM: https://www.openstreetmap.org

Mapbox: https://www.mapbox.com/

Leaflet: https://leafletjs.com



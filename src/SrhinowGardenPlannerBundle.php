<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

namespace Srhinow\GardenPlannerBundle;

use Srhinow\GardenPlannerBundle\DependencyInjection\SrhinowGardenPlannerBundleExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the Contao simple-map-bundle.
 */
class SrhinowGardenPlannerBundle extends Bundle
{
    /**
     * Builds the bundle.
     *
     * It is only ever called once when the cache is empty.
     *
     * This method can be overridden to register compilation passes,
     * other extensions, ...
     *
     * @param ContainerBuilder $container A ContainerBuilder instance
     */
    public function build(ContainerBuilder $container): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        return new SrhinowGardenPlannerBundleExtension();
    }
}

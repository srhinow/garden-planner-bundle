<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

//$GLOBALS['TL_DCA']['tl_content']['palettes']['simple_map_view'] = '
//    {type_legend},headline,type;
//    {module_legend},simpleMap;
//    {template_legend:hide},simpleMapTemplate;
//    {protected_legend:hide},protected;
//    {expert_legend:hide},guests,cssID,space;
//    {invisible_legend:hide},invisible,start,stop
//    ';

/*
 * Add fields to tl_content
 */

//$GLOBALS['TL_DCA']['tl_content']['fields']['simpleMap'] = [
//    'label' => &$GLOBALS['TL_LANG']['tl_content']['simpleMap'],
//    'exclude' => true,
//    'inputType' => 'select',
//    'foreignKey' => 'tl_simple_map.title',
//    'eval' => ['includeBlankOption' => true, 'chosen' => true, 'tl_class' => 'w50'],
//    'sql' => "varchar(64) NOT NULL default ''",
//];
//
//$GLOBALS['TL_DCA']['tl_content']['fields']['simpleMapTemplate'] = [
//    'label' => &$GLOBALS['TL_LANG']['tl_content']['simpleMapTemplate'],
//    'exclude' => true,
//    'inputType' => 'select',
//    'options' => \Contao\Backend::getTemplateGroup('ce_sm_'),
//    'eval' => ['tl_class' => 'w50'],
//    'sql' => "varchar(32) NOT NULL default ''",
//];

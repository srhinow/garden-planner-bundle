<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['TL_LANG']['tl_module']['item_template'] = ['Eintrag-Template', ''];
$GLOBALS['TL_LANG']['tl_module']['archive_mode'] = [
    'zeige alle veröffentlichten der Vergangenheit',
    'Es werden die Start und Stop-Angaben ignoriert, wenn zum Beispiel eine Archiv-Übersicht 
    über alle Einträge in der Vergangenheit angezeigt werden soll.',
];

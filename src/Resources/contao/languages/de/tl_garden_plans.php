<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

/*
 * Fields.
 */
$GLOBALS['TL_LANG']['tl_simple_map_category']['title'][0] = 'Titel';
$GLOBALS['TL_LANG']['tl_simple_map_category']['title'][1] = 'Titel der Kategorie';
$GLOBALS['TL_LANG']['tl_simple_map_category']['alias'][0] = 'Alias';
$GLOBALS['TL_LANG']['tl_simple_map_category']['alias'][1] = 'wird z.B. für die URL-Übergabe benötigt';
$GLOBALS['TL_LANG']['tl_simple_map_category']['sorting'][0] = 'Sortierung';
$GLOBALS['TL_LANG']['tl_simple_map_category']['sorting'][1] = 'Sortierungsplatz in einer eventuelle Listen-Darstellung';

/*
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_simple_map_category']['new'][0] = 'Neue Kategorie';
$GLOBALS['TL_LANG']['tl_simple_map_category']['new'][1] = 'Eine neue Kategorie anlegen.';
$GLOBALS['TL_LANG']['tl_simple_map_category']['edit'][0] = 'Kategorie bearbeiten';
$GLOBALS['TL_LANG']['tl_simple_map_category']['edit'][1] = 'Kategorie ID %s bearbeiten.';
$GLOBALS['TL_LANG']['tl_simple_map_category']['copy'][0] = 'Kategorie duplizieren';
$GLOBALS['TL_LANG']['tl_simple_map_category']['copy'][1] = 'Kategorie ID %s duplizieren.';
$GLOBALS['TL_LANG']['tl_simple_map_category']['delete'][0] = 'Kategorie löschen';
$GLOBALS['TL_LANG']['tl_simple_map_category']['delete'][1] = 'Kategorie ID %s löschen.';
$GLOBALS['TL_LANG']['tl_simple_map_category']['show'][0] = 'Kategoriedetails anzeigen';
$GLOBALS['TL_LANG']['tl_simple_map_category']['show'][1] = 'Details für Kategorie ID %s anzeigen.';

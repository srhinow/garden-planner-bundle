<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

/*
 * Content elements.
 */
$GLOBALS['TL_LANG']['CTE']['simple_map'] = ['OSM-Karten'];
$GLOBALS['TL_LANG']['CTE']['simple_map_view'] = [
    'Karten-Ansicht',
    'Stellt die aktiven Standorte auf einer Karte dar.',
];
$GLOBALS['TL_LANG']['CTE']['simple_map_category_list'] = [
    'Kategorie-Liste',
    'Zum Kategorie sortierten Anzeigen von Markierungsdaten.',
];

/*
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['simple_map_bundle'] = ['OSM-Karten'];
$GLOBALS['TL_LANG']['FMD']['simple_map_view'] = [
    'Karten-Ansicht',
    'Stellt die aktiven Standorte auf einer Karte dar.',
];
$GLOBALS['TL_LANG']['FMD']['simple_map_category_list'] = [
    'Kategorie-Liste',
    'Zum Kategorie sortierten Anzeigen von Markierungsdaten.',
];

/*
 * Back end modules
 */
$GLOBALS['TL_LANG']['MOD']['simple_map_bundle'] = ['OSM-Karten', 'Die einfache Karten-Verwaltung'];
$GLOBALS['TL_LANG']['MOD']['simple_map'] = [
    'Karten',
    'Hier können einfache Karten (OSM,Mapbox) verwaltet und dann per Content-Element 
    oder Frondent-Modul eingebunden werden.',
];
$GLOBALS['TL_LANG']['MOD']['simple_map_category'] = [
    'Kategorien',
    'Hier können für die Karten Markierungen Kategorien angelegt werden.',
];

<?php

declare(strict_types=1);

/*
 * This file is part of the Contao extension simple-map-bundle.
 *
 * (c) Sven Rhinow (sven@sr-tag.de)
 *
 * @license LGPL-3.0-or-later
 */

$GLOBALS['BE_GARDEN_PLANNER_BUNDLE']['PROPERTIES']['PUBLICSRC'] = 'bundles/srhinowgardenplanner';
/*
 * -------------------------------------------------------------------------
 * BACK END MODULES
 * -------------------------------------------------------------------------
 */
//if ('BE' === TL_MODE) {
//    $GLOBALS['TL_CSS'][] = $GLOBALS['BE_SIMPLE_MAP_BUNDLE']['PROPERTIES']['PUBLICSRC'].'/css/be.css|static';
//}

array_insert($GLOBALS['BE_MOD'], 1, ['garden_planner_bundle' => []]);

$GLOBALS['BE_MOD']['simple_map_bundle']['garden_plans'] = [
    'tables' => ['tl_garden_plans'],
    'icon' => $GLOBALS['BE_SIMPLE_MAP_BUNDLE']['PROPERTIES']['PUBLICSRC'].'/icons/category.png',
];

$GLOBALS['BE_MOD']['garden_planner_bundle']['garden_plants'] = [
    'tables' => ['tl_garden_plants'],
    'icon' => $GLOBALS['BE_SIMPLE_MAP_BUNDLE']['PROPERTIES']['PUBLICSRC'].'/icons/pin_map.png',
];


/*
 * -------------------------------------------------------------------------
 * CONTENT ELEMENT
 * -------------------------------------------------------------------------
 */
//array_insert($GLOBALS['TL_CTE'], 1, ['simple_map_bundle' => []]);
//$GLOBALS['TL_CTE']['simple_map']['simple_map_view'] = 'Srhinow\SimpleMapBundle\Elements\ContentSimpleMap';
//$GLOBALS['TL_CTE']['simple_map']['simple_map_category_list'] =
//    'Srhinow\SimpleMapBundle\Elements\ContentSimpleMapCategoryList';

/*
 * -------------------------------------------------------------------------
 * Front END MODULES
 * -------------------------------------------------------------------------
 */
//array_insert($GLOBALS['FE_MOD'], 2, [
//    'simple_map_bundle' => [
//        'simple_map_view' => 'Srhinow\SimpleMapBundle\Modules\Frontend\ModuleSimpleMapView',
//        'simple_map_category_list' => 'Srhinow\SimpleMapBundle\Modules\Frontend\ModuleSimpleMapCategoryList',
//    ],
//]);

/*
 * -------------------------------------------------------------------------
 * Models
 * -------------------------------------------------------------------------
 */
//$GLOBALS['TL_MODELS']['tl_simple_map'] = \Srhinow\SimpleMapBundle\Models\SimpleMapModel::class;
//$GLOBALS['TL_MODELS']['tl_simple_map_pin'] = \Srhinow\SimpleMapBundle\Models\SimpleMapPinModel::class;
//$GLOBALS['TL_MODELS']['tl_simple_map_category'] = \Srhinow\SimpleMapBundle\Models\SimpleMapCategoryModel::class;
